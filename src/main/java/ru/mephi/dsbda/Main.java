package ru.mephi.dsbda;


public class Main {

    public static void main(String[] args) {
        Router.routes(args.length != 0 ? args[0] : "--seeds");
        System.exit(0);
    }

}
