package ru.mephi.dsbda;

import ru.mephi.dsbda.database.migrations.CreateCitizensTable;
import ru.mephi.dsbda.database.migrations.CreateSalariesTable;
import ru.mephi.dsbda.database.migrations.CreateTripsTable;
import ru.mephi.dsbda.database.seeds.DatabaseSeeder;
import ru.mephi.dsbda.kafka.MainConsumer;
import ru.mephi.dsbda.kafka.MainProducer;
import ru.mephi.dsbda.models.Citizen;


import java.sql.SQLException;
import java.util.ArrayList;

class Router {
    private enum Routes {
        SEEDS,
        KAFKA,
        SPARK
    }


    static void routes(String route){
        try {
            Routes routes = Routes.valueOf(route.toUpperCase().substring(2));

            switch (routes) {
                case SEEDS:
                    try {
                        Router.seeds();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    break;
                case KAFKA:
                    Router.kafka();
                    break;
                case SPARK:
                    Router.spark();
                    break;
            }
        } catch (Exception e) {
            System.out.println("Error: route is not found!");
        }
    }

    private static void seeds() throws SQLException {
        DatabaseSeeder.run();

        System.out.println("SALARIES:");
        ArrayList<Citizen> salaries = Citizen.salaries();
        for(Citizen salary : salaries){
            System.out.println(salary);
        }

        System.out.println("TRIPS:");
        ArrayList<Citizen> trips = Citizen.trips();
        for(Citizen trip : trips){
            System.out.println(trip);
        }
    }

    private static void kafka() {
        MainProducer producer = new MainProducer();

        try {
            producer.run(Citizen.salaries());
            producer.run(Citizen.trips());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void spark() {
        MainConsumer consumer = new MainConsumer();
        consumer.run();
    }
}
