package ru.mephi.dsbda.configs;

public final class CassandraConfig extends Config {

    private String user;
    private String password;

    public CassandraConfig() {
        user = dotenv.get("CASSANDRA_USER");
        password = dotenv.get("CASSANDRA_PASSWORD");
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }
}
