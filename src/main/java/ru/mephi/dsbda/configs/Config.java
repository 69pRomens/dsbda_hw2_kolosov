package ru.mephi.dsbda.configs;

import io.github.cdimascio.dotenv.Dotenv;

class Config {
    final Dotenv dotenv = Dotenv.load();
}
