package ru.mephi.dsbda.database.migrations;

import ru.mephi.dsbda.connectors.MysqlConnector;

import java.sql.SQLException;

public class CreateCitizensTable {
    public static void up() throws SQLException {
        MysqlConnector mysql = new MysqlConnector();

        mysql.getConnection()
                .createStatement()
                .executeUpdate("CREATE TABLE citizens (" +
                        "id INT AUTO_INCREMENT NOT NULL," +
                        "passport VARCHAR(80) NOT NULL," +
                        "birthday TIMESTAMP NOT NULL," +
                        "PRIMARY KEY (id)" +
                        ")");

        mysql.closeConnection();

    }

    public static void down() throws SQLException {
        MysqlConnector mysql = new MysqlConnector();

        mysql.getConnection()
                .createStatement()
                .executeUpdate("DROP TABLE citizens");

        mysql.closeConnection();
    }
}
