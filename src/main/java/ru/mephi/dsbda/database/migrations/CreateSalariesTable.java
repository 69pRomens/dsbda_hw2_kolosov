package ru.mephi.dsbda.database.migrations;

import ru.mephi.dsbda.connectors.MysqlConnector;

import java.sql.SQLException;

public class CreateSalariesTable {
    public static void up() throws SQLException {
        MysqlConnector mysql = new MysqlConnector();

        mysql.getConnection()
                .createStatement()
                .executeUpdate("CREATE TABLE salaries (" +
                        "id INT AUTO_INCREMENT NOT NULL," +
                        "passport VARCHAR(80) NOT NULL," +
                        "month VARCHAR(80) NOT NULL," +
                        "salary VARCHAR(80) NOT NULL DEFAULT '0'," +
                        "PRIMARY KEY (id)" +
                        ")");

        mysql.closeConnection();

    }

    public static void down() throws SQLException {
        MysqlConnector mysql = new MysqlConnector();

        mysql.getConnection()
                .createStatement()
                .executeUpdate("DROP TABLE salaries");

        mysql.closeConnection();
    }
}
