package ru.mephi.dsbda.database.seeds;

import ru.mephi.dsbda.connectors.MysqlConnector;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Random;

public class CitizenSeeder {

    public static void generate() throws SQLException {
        MysqlConnector mysql = new MysqlConnector();

        Random random = new Random();
        int count = 50 + random.nextInt(100);

        String passport = "";

        long offset = Timestamp.valueOf("1971-01-01 00:00:00").getTime();
        long end = Timestamp.valueOf("2017-01-01 00:00:00").getTime();
        long diff = end - offset + 1;

        String query = "INSERT INTO citizens (passport, birthday) VALUES (?, ?)";
        PreparedStatement statement = mysql.getConnection().prepareStatement(query);

        for (int i = 0; i < count; i++) {
            passport = Integer.toString(420000000 + random.nextInt(490000000));
            Timestamp birthday = new Timestamp(offset + (long)(Math.random() * diff));

            statement.setString(1, passport);
            statement.setTimestamp(2, birthday);

            statement.addBatch();
        }

        statement.executeBatch();

        mysql.closeConnection();
    }
}
