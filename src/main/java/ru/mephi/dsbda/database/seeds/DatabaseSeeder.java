package ru.mephi.dsbda.database.seeds;

import ru.mephi.dsbda.database.migrations.CreateCitizensTable;
import ru.mephi.dsbda.database.migrations.CreateSalariesTable;
import ru.mephi.dsbda.database.migrations.CreateTripsTable;
import ru.mephi.dsbda.models.Citizen;

import java.sql.*;


public class DatabaseSeeder {

    public static void run() {
        try {
            CreateCitizensTable.down();
        } catch (SQLException e) {
            System.out.println("Error: Try drop not exists table!");
        }

        try {
            CreateSalariesTable.down();
        } catch (SQLException e) {
            System.out.println("Error: Try drop not exists table!");
        }

        try {
            CreateTripsTable.down();
        } catch (SQLException e) {
            System.out.println("Error: Try drop not exists table!");
        }

        try {
            CreateCitizensTable.up();
            CreateSalariesTable.up();
            CreateTripsTable.up();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            CitizenSeeder.generate();
            SalarySeeder.generate();
            TripSeeder.generate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
