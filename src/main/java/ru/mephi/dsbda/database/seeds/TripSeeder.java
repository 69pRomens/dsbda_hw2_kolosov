package ru.mephi.dsbda.database.seeds;

import ru.mephi.dsbda.connectors.MysqlConnector;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class TripSeeder {

    public static void generate() throws SQLException {
        MysqlConnector mysql = new MysqlConnector();

        Random random = new Random();
        List<String> months = Arrays
                .asList("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

        String passport = "";
        String month = "";

        ResultSet result;

        result = mysql.getConnection()
                .createStatement()
                .executeQuery("SELECT * FROM citizens");

        String query = "INSERT INTO trips (passport, month, trips) VALUES (?, ?, ?)";
        PreparedStatement statement = mysql.getConnection().prepareStatement(query);

        while(result.next()) {

            int trips = random.nextInt(5);

            if (trips > 0) {
                passport = result.getString("passport");
                month = months.get(random.nextInt(months.size()));

                statement.setString(1, passport);
                statement.setString(2, month);
                statement.setInt(3, trips);

                statement.addBatch();
            }
        }

        statement.executeBatch();

        mysql.closeConnection();
    }
}
