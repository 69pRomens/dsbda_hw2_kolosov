package ru.mephi.dsbda.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import ru.mephi.dsbda.models.Citizen;

import java.util.Collections;
import java.util.Properties;

public class MainConsumer {

    private final String topic = "citizens1";
    private Properties props = new Properties();


    public MainConsumer () {
        String deserializer = StringDeserializer.class.getName();
        props.put("bootstrap.servers", "172.17.0.2:6667");
        props.put("group.id", "hw2");
        props.put("auto.offset.reset", "earliest");
        props.put("key.deserializer", deserializer);
        props.put("value.deserializer", deserializer);
    }

    public void run() {
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singletonList(topic));

        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(1000);

            if (records.isEmpty()) continue;

            for (ConsumerRecord<String, String> record : records) {
                Citizen citizen = new Citizen(record.value());
                System.out.println(citizen);
            }
        }
    }
}
