package ru.mephi.dsbda.kafka;


import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import ru.mephi.dsbda.models.Citizen;


import java.sql.SQLException;
import java.util.*;

public class MainProducer {

    private final String topic = "citizens1";
    private Properties props = new Properties();


    public MainProducer () {
        String serializer = StringSerializer.class.getName();
        props.put("bootstrap.servers", "172.17.0.2:6667");
        props.put("key.serializer", serializer);
        props.put("value.serializer", serializer);
    }

    public void run (ArrayList<Citizen> citizens) {
        final Iterator<Citizen> iterator = citizens.iterator();

        try {
            Producer<Object, String> producer = new KafkaProducer<>(props);
            while(iterator.hasNext()){
                // Send the sentence to the test topic
                producer.send(new ProducerRecord<>(topic, iterator.next().toString()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
