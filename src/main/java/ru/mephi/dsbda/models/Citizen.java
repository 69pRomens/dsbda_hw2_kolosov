package ru.mephi.dsbda.models;

import ru.mephi.dsbda.connectors.MysqlConnector;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

public class Citizen {

    private int id;
    private String passport;
    private Timestamp birthday;
    private String month;
    private String salary;
    private int trips;

    public Citizen() {

    }

    public Citizen(String value) {
        String[] tokens = value.split(";");
        this.id = Integer.parseInt(tokens[0]);
        this.passport = tokens[1];
        this.birthday = Timestamp.valueOf(tokens[2]);
        this.month = tokens[3];
        this.salary = tokens[4];
        this.trips = Integer.parseInt(tokens[5]);
    }

    public Citizen(int id, String passport, Timestamp birthday, String month, String salary, int trips) {
        this.id = id;
        this.passport = passport;
        this.birthday = birthday;
        this.month = month;
        this.salary = salary;
        this.trips = trips;
    }

    public Citizen(int id, String passport, Timestamp birthday) {
        this.id = id;
        this.passport = passport;
        this.birthday = birthday;
    }

    public Citizen(String passport, String month, String salary) {
        this.passport = passport;
        this.month = month;
        this.salary = salary;
    }

    public Citizen(String passport, String month, int trips) {
        this.passport = passport;
        this.month = month;
        this.trips = trips;
    }

    public static ArrayList<Citizen> all() throws SQLException {
        MysqlConnector mysql = new MysqlConnector();

        ResultSet result;

        result = mysql.getConnection()
                .createStatement()
                .executeQuery("SELECT * FROM citizens");

        ArrayList<Citizen> list = new ArrayList<Citizen>();

        while(result.next()) {
            int id = result.getInt("id");
            String passport = result.getString("passport");
            Timestamp birthday = result.getTimestamp("birthday");

            Citizen citizen = new Citizen(id, passport, birthday);

            list.add(citizen);
        }

        mysql.closeConnection();

        return list;
    }

    public static ArrayList<Citizen> salaries() throws SQLException {
        MysqlConnector mysql = new MysqlConnector();

        ResultSet result;

        result = mysql.getConnection()
                .createStatement()
                .executeQuery("SELECT * FROM salaries");

        ArrayList<Citizen> list = new ArrayList<Citizen>();

        while(result.next()) {
            String passport = result.getString("passport");
            String month = result.getString("month");
            String salary = result.getString("salary");

            Citizen citizen = new Citizen(passport, month, salary);

            list.add(citizen);
        }

        result = mysql.getConnection()
                .createStatement()
                .executeQuery("SELECT * FROM citizens");

        while(result.next()) {
            for (Citizen element : list) {
                if (element.passport.equals(result.getString("passport"))) {
                    element.setId(result.getInt("id"));
                    element.setBirthday(result.getTimestamp("birthday"));
                }
            }
        }

        mysql.closeConnection();

        return list;
    }

    public static ArrayList<Citizen> trips() throws SQLException {
        MysqlConnector mysql = new MysqlConnector();

        ResultSet result;

        result = mysql.getConnection()
                .createStatement()
                .executeQuery("SELECT * FROM trips");

        ArrayList<Citizen> list = new ArrayList<Citizen>();

        while(result.next()) {
            String passport = result.getString("passport");
            String month = result.getString("month");
            int trips = result.getInt("trips");

            Citizen citizen = new Citizen(passport, month, trips);

            list.add(citizen);
        }

        result = mysql.getConnection()
                .createStatement()
                .executeQuery("SELECT * FROM citizens");

        while(result.next()) {
            for (Citizen element : list) {
                if (element.passport.equals(result.getString("passport"))) {
                    element.setId(result.getInt("id"));
                    element.setBirthday(result.getTimestamp("birthday"));
                }
            }
        }

        mysql.closeConnection();

        return list;
    }

    public Timestamp getBirthday() {
        return birthday;
    }

    public void setBirthday(Timestamp birthday) {
        this.birthday = birthday;
    }

    public int getTrips() {
        return trips;
    }

    public void setTrips(int trips) {
        this.trips = trips;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return ""
                + id + ";"
                + passport + ";"
                + birthday + ";"
                + month + ";"
                + salary + ";"
                + trips + ";";
    }
}
